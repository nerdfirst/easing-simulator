# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Easing - Friday Minis 295**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/cs-Ph5knR6M/0.jpg)](https://www.youtube.com/watch?v=cs-Ph5knR6M "Click to Watch")


# In this Repository

The **EF Simulator** directory contains the source code for the interactive Easing Factor simulator hosted on the NERDfirst website. You can see the live version here: http://resources.nerdfirst.net/easing.

The **Video Demos** directory contains the source code for the demos shown in the video. These have just been put together quickly, but are included for completeness. In particular, I don't recommend running "Demo3.html" for very long as it uses an inefficient way of appending line segments onscreen.


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.