var points = [];
var x = 0;

var maxEf = 1;

for (var i=0; i<200; i++) {
    points.push(0);
}

window.onload = function() {
    scaleCanvas();
    window.onresize = scaleCanvas;

    get("#canvas").onmousemove = mouse;
    get("#canvas").onwheel = zoom;
	
	window.onkeydown = function(e) {
		if (e.keyCode == 27) {
			get("#help").style.display = "none";
		}
	}

    window.requestAnimationFrame(animate);
}

function scaleCanvas() {
    get("#canvas").setAttribute("width", window.innerWidth);
    get("#canvas").setAttribute("height", window.innerHeight);

    redrawGrid();
}

function redrawGrid() {
    var interval = window.innerHeight / 5;
    var parts = get("#grid").childNodes;
    
    for (var i=1; i<5; i++) {
        parts[i].setAttribute("x1", 0);
        parts[i].setAttribute("x2", window.innerWidth);
        parts[i].setAttribute("y1", i*interval);
        parts[i].setAttribute("y2", i*interval);
    }

    parts = get("#labels").childNodes;
    for (var i=1; i<=5; i++) {
        parts[i].setAttribute("x", 10);
        parts[i].setAttribute("y", (i-1)*interval+20);
    }

    labelGrid();
}

function mouse(e) {
    x = e.clientX;

    var mouseLine = get("#mouseLine");
    mouseLine.setAttribute("x1", x);
    mouseLine.setAttribute("y2", window.innerHeight);
    mouseLine.setAttribute("x2", x);
}

function zoom(e) {
    var delta;
	if (e.deltaY > 0) delta = 0.03;
	else if (e.deltaY < 0) delta = -0.03;
	else delta = 0;
	
    maxEf -= delta;

    if (maxEf < 0) { maxEf = 0; }
    if (maxEf > 1) { maxEf = 1; }

    labelGrid();
	
	var roundedEf = Math.round(maxEf*100) / 100;
	get("#maxEF").innerHTML = roundedEf;
}

function labelGrid() {
    parts = get("#labels").childNodes;
    for (var i=1; i<=5; i++) {
        var val = maxEf * ((6-i)/5);
        parts[i].innerHTML = val.toFixed(2);
    }
}

function animate() {
    for (var i=0; i<200; i++) {
        var ef = ((i+1)*maxEf) / 200;
        points[i] += (x - points[i]) * ef;
    }

    var ht = window.innerHeight;
    var coords = "";
    for (var i=0; i<200; i++) {
        var y = (ht/200) * (200-i);
        coords += points[i] + "," + y + " ";
    }
    get("#mainLine").setAttribute("points", coords);

    window.requestAnimationFrame(animate);
}

function get(item) {
    var result = document.querySelectorAll(item);
    if (result.length == 0) {
        return null;
    }
    else if (result.length == 1) {
        return result[0];
    }
    else {
        return result;
    }
}